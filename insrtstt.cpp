#include<stdio.h>
#include<iostream>
#include<bits/stdc++.h>
using namespace std;
void insertion_srt(int a[],int n)
{
    int i ,j, item;
    for(i=1 ; i<n; i++)
    {
        item = a[i];
        j= i-1;
        while(j>=0  && a[j] > item)
        {
            a[j+1] = a[j];
            j= j-1;
        }
        a[j+1]=item;
    }
}


int main()
{
    int n, a[100], i;
    cin>>n;
    for(i=0; i<n; i++)
    {
        cin>>a[i];
    }
    printf("\n");
    printf("Before Sorting\n");

    for(i = 0; i < n; i++)
    {

        printf("%d ", a[i]);

    }
     printf("\n");

    printf("After Sorting\n");

    insertion_srt(a,n);

    for(i = 0; i < n; i++)
    {

        printf("%d ", a[i]);

    }
    printf("\n");
}
