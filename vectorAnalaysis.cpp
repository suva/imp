#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;
int main()
{
    vector <int> v;

    v.push_back(5);
    v.push_back(4);
    v.push_back(3);
    v.push_back(2);
    v.push_back(1);

    cout<< "Vector Size = " <<v.size()<< endl;  /// vector size
    cout<< "Vector First Element = " << v[0] << endl; /// vector first element
    sort(v.begin(), v.end());/// vector sort
     cout<<"After sorting " << endl; //
    for(int i = 0; i < v.size(); i++)
    {
        cout<< v[i] << " ";
    }
    cout<< endl;
    vector <int> :: iterator it; /// special pointer

    it = max_element(v.begin(), v.end());
    cout<<"Max Element =  " << *it << endl;
     int pos = 2;
    it = find(v.begin(), v.end(), v[pos]); ///  deleting position 2

    v.erase(it);
    cout<<"After Deleting " << endl;
    for(int i = 0; i < v.size(); i++)
    {
        cout<< v[i] << " ";
    }
    cout<< endl;
    v.insert(it, 100); /// insert 100 in position 2
    cout<<"After Insert "<< endl;

    for(int i = 0; i < v.size(); i++)
    {
        cout<< v[i] << " ";
    }
    cout<<endl;


}
